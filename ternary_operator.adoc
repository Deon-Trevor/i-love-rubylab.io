=== ? :

The `? :` is called ternary operator. It can be used as a simple `if`. Take the program shown below. Concentrate on `max = a > b  ? a : b`

[source, ruby]
----
include::code/max_of_nums.rb[]
----

When executed the program gives the following output

----
"max = 5"
----

Well the ?: works as follows. Its syntax is like this

----
<evaluate something > ? <if true take this thing> : <if false take this thing>
----

You give an expression before the question mark. This expression must either return `true` or `false`. If the expression returns true `it` returns the stuff between `?` and  `:` , if `false` it returns the stuff after `:`

In the expression

----
max = a > b  ? a : b
----

We can substitute the values of `a` and `b` as follows

----
max = 3 > 5  ? 3 : 5
----

3 is not greater than 5, hence its `false`. Hence, the value after `:` is assigned to `max`. Hence, `max` becomes 5.

