== Breaking large programs

It's not that you will be writing professional programs that are all in a single file. You need to break them up into small chunks, put those chunks into separate files and include them in other programs as one needs. So lets see an example

.{code_url}/break_full.rb[break_full.rb]
[source, ruby]
----
include::code/break_full.rb[]
----

Output

----
The squares perimeter is 20
----

So you see the above program named link:code:break_full.rb[break_full.rb], that has a class definition and then a snippet of code that uses the definition to calculate perimeter of square of side 5 units.

Isn't it logical that if the `Square` code can go into a separate file, so that it can be required where it needs to be, possibly in many other programs? If a program gets large we can divide them up into smaller files and name them logically so that its easy to read, reuse and debug.

So following this principle, I have broken this program into two, the first one is link:code/break_square.rb[break_square.rb] as shown below, this just has the `Square` class definition

.{code_url}/break_square.rb[break_square.rb]
[source, ruby]
----
include::code/break_square.rb[]
----

Now see the program called link:code/break_main.rb[break_main.rb] below,

.{code_url}/break_main.rb[break_main.rb]
[source, ruby]
----
include::code/break_main.rb[]
----

Output

----
The squares perimeter is 20
----

See the line `require "./break_square.rb"``, now that does the trick, the `./break_square.rb` represents the path where {code_url}/break_sqare.rb[break_square.rb] is located. The `./` means search in this very folder. So once the program gets the file {code_url}/break_sqare.rb[break_square.rb], it simply kinda inserts the code in that position and works the same as {code_url}/break_full.rb[break_full.rb], but this time the code is logically divide and possibly easy to maintain.
