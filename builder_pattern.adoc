== Builder Pattern

Let's say that you are writing a piece of software for a computer maker whose website offers very high degree of customization. A computer can have lots of parts that can be varied, so there are a lot of combinations. So rather than put all these code in just one single huge computer class, we create a class called builder, in this case we create a class called `ComputerBuilder`, which takes on the task of creating a customized computer and possibly eases the creation or build process too.

Let's say that we need different types of CPU's to build a computer, for that we write three types of CPU classes as shown below in link:code/design_patterns/builder/cpus.rb[cpus.rb].

[source, ruby, linenums]
----
include::code/design_patterns/builder/cpus.rb[]
----

Computers may need drives, we write a class for it too, if you see the code link:code/design_patterns/builder/drive.rb[drive.rb] below, for the drive, we have provided a class, along with an initializer which we can use to customize it to a degree.

[source, ruby, linenums]
----
include::code/design_patterns/builder/drive.rb[]
----

Similarly, below we have coded for the mother board in link:code/design_patterns/builder/motherboard.rb[motherboard.rb].

[source, ruby, linenums]
----
include::code/design_patterns/builder/motherboard.rb[]
----

Now lets look at the `Computer` class (in link:code/design_patterns/builder/computer.rb[computer.rb]), this is very similar to class `Drive` and `Motherboard`. Our aim is to understand the builder pattern, so lets move on.

[source, ruby, linenums]
----
include::code/design_patterns/builder/computer.rb[]
----

Let's now analyze the hero of this section, the `ComputerBuilder` class. Go through the code below, we will talk about it soon.

[source, ruby, linenums]
----
include::code/design_patterns/builder/computer_builder.rb[]
----

In the program above we do have `initialize` method in line 4, but if you look at line 44, in method `computer` is where the `Computer` instance is returned. In that very same function if you see we have code to raise exceptions if there aren't enough memory, or if too many disks are added to computer. So this is another advantage of the Builder Pattern where you can prevent a complex object getting built if it does not satisfy certain criteria.

In the builder you have methods like `display`, `memory_size`, and a function called `turbo` to set parameters for display, memory and the type of CPU. We also have functions like `add_cd`, `add_dvd` and `add_hard_disk`, to add these things to the computer we are building it.

Now lets look at the program that brings all together. Take a look at the program link:code/design_patterns/builder/main.rb[main.rb] below, type it or copy-paste and execute it.

[source, ruby, linenums]
----
include::code/design_patterns/builder/main.rb[]
----

Output

----
#<Computer:0x0000564155728610>
#<Computer:0x0000564155728598>
#<Computer:0x0000564155728570>
#<Computer:0x0000564155728548>
#<Computer:0x0000564155728520>
#<Computer:0x00005641557284f8>
#<Computer:0x00005641557284d0>
#<Computer:0x00005641557284a8>
#<Computer:0x0000564155728430>
#<Computer:0x0000564155728408>
Not enough memory.
Too many drives.
Computer built with magic method builder
CPU: TurboCPU
Drive: cd
Drive: dvd
Drive: hard_disk
----

Now lets see how it works. From lines 1-5:

[source, ruby]
----
require_relative 'cpus'
require_relative 'drive'
require_relative 'motherboard'
require_relative 'computer'
require_relative 'computer_builder'
----

We require the necessary files for this program to work. In the following lines shown below we create a `ComputerBuilder` instance called `builder` which has a turbo CPU, LCD, has one CD player, has a writable DVD and a hard disk of 100,000 megabytes:

[source, ruby]
----
builder = ComputerBuilder.new
builder.turbo
builder.display(:lcd)
builder.add_cd
builder.add_dvd(true)
builder.add_hard_disk(100_000)
----

Now using this `builder`, we can clonefootnote:[https://ruby-doc.org/core-2.7.0/Object.html#method-i-clone] it to create any number of computers. In the code below we create 10 computer clones and print them

[source, ruby]
----
# manufacture 10 computers using the builder
computers = []
10.times { computers << builder.computer.clone }
computers.each { |computer| puts computer }
----

NOTE: Imagine you are writing a game, and you need to create tens of objects, this pattern will be handy.

Now lets test the code that will raise exception if the memory is low. Look at the code below

[source, ruby]
----
# computer must have at least 250 MB of memory
builder = ComputerBuilder.new
builder.memory_size(249)
begin
  builder.computer
rescue Exception => e
  puts e.message
end
----

In the code above we create a computer builder in `builder = ComputerBuilder.new`, next we deliberately assign low memory in this line `builder.memory_size(249)`, in the `begin` `end` block we try to create a `Computer` instance from the `builder` using `builder.computer` and it sure raises an exception which is caught and printed out by this `puts e.message` statement under `rescue`. In the output we would have got this message:

----
Not enough memory.
----

Similarly, we try to build a computer having five drives, that is 2 CD's, 2 DVD's and 1 Hard Disk. We have limited maximum number of drives to 4, in `ComputerBuilder` class, so the code snippet below will raise an error:

[source, ruby]
----
# computer must have at most 4 drives
builder = ComputerBuilder.new
builder.add_cd
builder.add_dvd
builder.add_hard_disk(1000)
builder.add_cd
builder.add_dvd
begin
  builder.computer
rescue Exception => e
  puts e.message
end
----

This error is caught and printed and hence we get the following output:

----
Too many drives.
----

=== Exercise

You now know about builder patter. If you like to explore further, take a look at the code below

[source, ruby]
----
# use magic method to rapidly build a computer
puts 'Computer built with magic method builder'
builder = ComputerBuilder.new
builder.add_cd_and_dvd_and_harddisk_and_turbo
computer = builder.computer
puts "CPU: #{computer.motherboard.cpu.class}"
computer.drives.each { |drive| puts "Drive: #{drive.type}" }
----

This code produces the following output

----
Computer built with magic method builder
CPU: TurboCPU
Drive: cd
Drive: dvd
Drive: hard_disk
----

Now in `ComputerBuilder` class take a look at the `method_missing` method, and try to explain how this `builder.add_cd_and_dvd_and_harddisk_and_turbo` statement works.
