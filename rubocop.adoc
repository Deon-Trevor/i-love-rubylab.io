== Rubocop

In the last section you saw about Ruby style guides. You can read it, have it in your memory and try to code your Ruby programs so that they have a standard. By following style guides the difference in the way one writes a program from other is reduced and hence maintainability improves.

There is another way to implement style guides, and tits by using a gem called Rubocop. Install the gem using the following command in terminal

```
$ gem install rubocop
```

Now let's create a program that can be linted better

.{code_url}/rubocop_example.rb[rubocop_example.rb]
```ruby
include::code/rubocop_example.rb[]
```

Now we save it and run Rubocop on it as shown:

```
$ rubocop rubocop_example.rb
```

rubocop spits out some errors as shown:

```
Inspecting 1 file
C

Offenses:

rubocop_example.rb:1:1: C: [Correctable] Style/FrozenStringLiteralComment: Missing frozen string literal comment.
name = "Karthik"
^
rubocop_example.rb:1:8: C: [Correctable] Style/StringLiterals: Prefer single-quoted strings when you don't need string interpolation or special symbols.
name = "Karthik"
       ^^^^^^^^^

1 file inspected, 2 offenses detected, 2 offenses auto-correctable
```

I duckduckgoed Style/FrozenStringLiteralComment rubocop and found out that it's a better practice to include single quotes than using double quotes to enclose a string from here https://www.rubydoc.info/gems/rubocop/RuboCop/Cop/Style/StringLiterals

As you can see below, this was the page that duckduckgo took me to:

image::rubocop-e8c5a.png[]

one could find entire Rubocop list of rules here https://www.rubydoc.info/gems/rubocop/

image::rubocop-30b9f.png[]

I also corrected another error Style/FrozenStringLiteralComment, and saved the code as shown below:

.{code_url}/rubocop_example_corrected.rb[rubocop_example_corrected.rb]
```ruby
include::code/rubocop_example_corrected.rb[]
```

Now when I ran Rubocop on the corrected code I got no linting suggestions as shown:

```
$ rubocop rubocop_example_corrected.rb
```

Output

```
Inspecting 1 file
.

1 file inspected, no offenses detected

```

One could use `rubocop -A <file-name>` for Rubocop to automatically implement linting suggestions in a ruby file. If you are using a text editor or IDE to code, you may also explore how to integrate Rubocop into it, and let Rubocop automatically lint your file when ever you save it.
