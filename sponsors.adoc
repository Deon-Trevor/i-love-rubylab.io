== Sponsors

++++

<p>
  <a href="https://doneit.com" target="_blank">
    <img src="https://bafybeiddlyyrzrkzi2rfevguhyjssvajt46fzgofjs5n56xnaei6ciyqdy.ipfs.dweb.link/doneit.png" />
  </a>
</p>

++++

== Donate

To keep up my work and to update the book regularly, please consider donating. You may donate to me via PayPal here https://www.paypal.me/mindaslab
