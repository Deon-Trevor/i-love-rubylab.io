=== case when and matcher classes

To understand the program below, you need to read === and case when, checking the class type. Type the program below and execute it

[source, ruby]
----
include::code/case_when_matcher_classes.rb[]
----

Output

----
Nice to meet you Zigor!!!
----

Consider this section

[source, ruby]
----
case name
when Zigor
  puts "Nice to meet you Zigor!!!"
else
  puts "Who are you?"
end
----

The `case` statement checks `name` and sees if its instance of class type `Zigor`. Well isn't that surprising?  How could that be? How can one instance that belongs to `String` class become that of `Zigor` class? Well, what the `case` `when` does is this, it invokes `===` method in class `Zigor`, its definition could be seen below footnote:[To understand these stuff you must be familiar in Functions and Classes & Objects].
 
[source, ruby]
----
def self.===(string)
  string.downcase == "zigor"
end
----

Here we define `self.===` , where we take in an argument string , here the `name` passed to `case` gets copied to `string`, and it checks if the `downcase` of `string` is equal to `“zigor”` in  `string.downcase == "zigor"` , if yes, it returns `true`, else `false`. If `true` the code in the when Zigor block gets executed and we get the output `Nice to meet you Zigor!!!` . Change name to other values and see what happens.

Don't worry if you do not understand this section now. After completing this book revisit it, you might be in a better state to understand it.


