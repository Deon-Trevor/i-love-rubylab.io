== Copyright

Copyright (c) 2009 - End of Universe, {author}

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license can be found in http://www.gnu.org/copyleft/fdl.html 

All code in this book is released under GPL V3 footnote:[https://www.gnu.org/copyleft/gpl.html ] or later.

