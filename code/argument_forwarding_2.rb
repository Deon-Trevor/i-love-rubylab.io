def print_something(string)
  puts string
end

def decorate(..., function)
  puts "#" * 50
  function(...)
  puts "#" * 50
end

decorate("Hello World!", print_something) # does not work

# $ ruby argument_forwarding_2.rb
# argument_forwarding_2.rb:5: syntax error, unexpected ',', expecting ')'
# def decorate(..., function)

