require 'date'

dates = Enumerator.produce(Date.today, &:next) #=> infinite sequence of dates
p dates.detect(&:tuesday?) #=> next tuesday

