array = ["A", "B", "C", "B", "A"]

p array
  .group_by { |v| v }
  .map { |k, v| [k, v.size] }
  .to_h

p  array
  .group_by { |v| v }
  .transform_values(&:size)

p array.each_with_object(Hash.new(0)) { |v, h| h[v] += 1 }

# now Enumerable#tally

p array.tally #=> {"A"=>2, "B"=>2, "C"=>1}
