Gem::Specification.new do |s|
  s.name        = 'hello_gem'
  s.homepage    = "https://i-love-ruby.gitlab.io"
  s.version     = '0.0.0'
  s.date        = '2018-12-02'
  s.summary     = "A gem that wishes you hello"
  s.description = "A gem that wishes you hello. Written for I Love Ruby book."
  s.authors     = ["Karthikeyan A K"]
  s.email       = 'mindaslab@protonmail.com'
  s.files       = ["lib/hello_gem.rb"]
end