=== loop

So we have seen many types of loops till now, but I have left out a basic loop which is we call `loop`. Why I have left it out Because it's dangerous to use footnote:[If you run this loop a million times then it will automatically trigger nuclear warheads in Area 51. This will make Russia counteract thus unleashing a nuclear Armageddon]. Okay lets see an example. Type the program below and execute it. Note that you need to press Ctrl+C to stop it executing. So be cautious

[source, ruby]
----
include::code/loop.rb[]
----

---- 
Output 
I Love Ruby 
I Love Ruby 
I Love Ruby 
I Love Ruby 
I Love Ruby 
I Love Ruby 
I Love Ruby 
I Love Ruby 
I Love Ruby 
I Love Ruby 
I Love Ruby 
......
----

The output will keep on printing `I Love Ruby` until u press Ctrl and C keys together to break. The basic is this: Anything put between loop do and end will keep on going.

So now lets say that we don't want this loop to be continuously running forever. Let's see how to tame it. Let's print a program that prints from 1 to 10. Type the program below and run it.

[source, ruby]
----
include::code/break_at_10.rb[]
----

Output

----
1 
2 
3 
4 
5 
6 
7 
8 
9 
10
----

So the program prints from 1 to 10 as we wished. Let's walk through it and see how it works. The first line `i = 1`, stores the value `1` in variable named `i`. Next we have this `loop do` line where anything put between this and `end` will run continuously.
 
In the next line `puts i`, we print the value of `i` and hence `1` gets printed, now in `break if i == 10`, it checks if `i` is 10, here the condition is `false` as `i` is not 10, hence the loop will continue to the next statement `i = i + 1`, we are adding 1 to `i` in `i + 1` and hence its becomes 2 so by saying `i = i + 1` we mean `i = 1 + 1` hence `i` will become 2 and it (the program) meets the `end` statement, it does not mean end it just means iteration of loop ends so return to top (that is to `loop do`) once again.

So in loop do, now `i` is 2, so the thing goes on and on till `i` is `10` and in that case, in `break if i == 10`, `i == 10` becomes `true` and the loop breaks. 

*Exercise*: Try modifying the link:code/break_at_10.rb[break_at_10.rb], when we have finished printing 10, the program must print "Mom I have finished printing 10"

*Answer:* link:code/telling_mom.rb[telling_mom.rb]
 
*Exercise:* These western guys don't like 13, so write a program to print from 1 10 20, but omit 13. 
Answer: link:code/no_13.rb[no_13.rb]

*Exercise:* Explain if link:code/no_13_a.rb[no_13_a.rb] will work. If ya, how? If nay, why not? footnote:[If you can't find out, you will know it on 13-14-2020 AD, that's the judgement day. God will descend upon earth and give answer to this ultimate question. If you want to know watch “The Hitchhiker's Guide to the Galaxy” which contains a very secret message. DONTPANIC.]

